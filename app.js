let dbTimer, tlTimer

let dbCounter = 0
let tlCounter = 0

function taskRunner (type, delay) {
  // display tip
  if (type === 'debounce') {
    console.warn('Debounced function only fires after ' + delay + 'ms from LAST trigger.')
  } else if (type === 'throttle') {
    console.warn('Throttled function only fires after ' + delay + 'ms from FIRST trigger.')
  } else {
    console.warn('Normal function fires as often as you click.')
  }

  // run function
  console.log('Running ' + type + ' task...')
  console.log('Task cleared. Waiting for next input. 👀⌛\n ')
}

function debouncer (done, delay, type) {
  console.log('Debouncer clicked ' + ++dbCounter + ' times')
  clearTimeout(dbTimer)
  // console.log(dbTimer)

  dbTimer = setTimeout(function () {
    done(type, delay)
    dbCounter = 0
  }, delay)
}

function throttler (done, delay, type) {
  console.log('Throttler clicked ' + ++tlCounter + ' times')
  // console.log(tlTimer)

  if (tlTimer) {
    // exits throttler function if timer is already running
    return
  }

  tlTimer = setTimeout(function () {
    done(type, delay)
    tlTimer = 0
    // clearTimeout(tlTimer) resolves timer to 1
    tlCounter = 0
  }, delay)
}
